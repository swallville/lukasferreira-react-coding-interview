module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/atoms/ErrorBoundary/index.tsx":
/*!**************************************************!*\
  !*** ./components/atoms/ErrorBoundary/index.tsx ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ErrorBoundary; });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ \"antd\");\n/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_2__);\n\nvar _jsxFileName = \"/Users/lukas.machado/Downloads/lukasferreira-react-coding-interview/components/atoms/ErrorBoundary/index.tsx\";\n\n\nclass ErrorBoundary extends react__WEBPACK_IMPORTED_MODULE_1___default.a.Component {\n  constructor(props) {\n    super(props);\n    this.state = {\n      hasError: false\n    };\n  }\n\n  static getDerivedStateFromError(error) {\n    return {\n      hasError: true\n    };\n  }\n\n  componentDidCatch(error, errorInfo) {\n    antd__WEBPACK_IMPORTED_MODULE_2__[\"message\"].error('Oops! Looks like something bad happened. Try again', 2);\n  }\n\n  render() {\n    if (this.state.hasError) {\n      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(\"h1\", {\n        children: \"Something went wrong.\"\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 27,\n        columnNumber: 14\n      }, this);\n    }\n\n    return this.props.children;\n  }\n\n}//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL2F0b21zL0Vycm9yQm91bmRhcnkvaW5kZXgudHN4PzBlOGUiXSwibmFtZXMiOlsiRXJyb3JCb3VuZGFyeSIsIlJlYWN0IiwiQ29tcG9uZW50IiwiY29uc3RydWN0b3IiLCJwcm9wcyIsInN0YXRlIiwiaGFzRXJyb3IiLCJnZXREZXJpdmVkU3RhdGVGcm9tRXJyb3IiLCJlcnJvciIsImNvbXBvbmVudERpZENhdGNoIiwiZXJyb3JJbmZvIiwibWVzc2FnZSIsInJlbmRlciIsImNoaWxkcmVuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFDQTtBQU1lLE1BQU1BLGFBQU4sU0FBNEJDLDRDQUFLLENBQUNDLFNBQWxDLENBR2I7QUFDQUMsYUFBVyxDQUFDQyxLQUFELEVBQVE7QUFDakIsVUFBTUEsS0FBTjtBQUNBLFNBQUtDLEtBQUwsR0FBYTtBQUFFQyxjQUFRLEVBQUU7QUFBWixLQUFiO0FBQ0Q7O0FBRUQsU0FBT0Msd0JBQVAsQ0FBZ0NDLEtBQWhDLEVBQXVDO0FBQ3JDLFdBQU87QUFBRUYsY0FBUSxFQUFFO0FBQVosS0FBUDtBQUNEOztBQUVERyxtQkFBaUIsQ0FBQ0QsS0FBRCxFQUFRRSxTQUFSLEVBQW1CO0FBQ2xDQyxnREFBTyxDQUFDSCxLQUFSLENBQWMsb0RBQWQsRUFBb0UsQ0FBcEU7QUFDRDs7QUFFREksUUFBTSxHQUFHO0FBQ1AsUUFBSSxLQUFLUCxLQUFMLENBQVdDLFFBQWYsRUFBeUI7QUFDdkIsMEJBQU87QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FBUDtBQUNEOztBQUVELFdBQU8sS0FBS0YsS0FBTCxDQUFXUyxRQUFsQjtBQUNEOztBQXBCRCIsImZpbGUiOiIuL2NvbXBvbmVudHMvYXRvbXMvRXJyb3JCb3VuZGFyeS9pbmRleC50c3guanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgbWVzc2FnZSB9IGZyb20gJ2FudGQnO1xuXG5pbnRlcmZhY2UgRXJyb3JCb3VuZGFyeVN0YXRlIHtcbiAgaGFzRXJyb3I6IGJvb2xlYW47XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVycm9yQm91bmRhcnkgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8XG4gIHt9LFxuICBFcnJvckJvdW5kYXJ5U3RhdGVcbj4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0geyBoYXNFcnJvcjogZmFsc2UgfTtcbiAgfVxuXG4gIHN0YXRpYyBnZXREZXJpdmVkU3RhdGVGcm9tRXJyb3IoZXJyb3IpIHtcbiAgICByZXR1cm4geyBoYXNFcnJvcjogdHJ1ZSB9O1xuICB9XG5cbiAgY29tcG9uZW50RGlkQ2F0Y2goZXJyb3IsIGVycm9ySW5mbykge1xuICAgIG1lc3NhZ2UuZXJyb3IoJ09vcHMhIExvb2tzIGxpa2Ugc29tZXRoaW5nIGJhZCBoYXBwZW5lZC4gVHJ5IGFnYWluJywgMik7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgaWYgKHRoaXMuc3RhdGUuaGFzRXJyb3IpIHtcbiAgICAgIHJldHVybiA8aDE+U29tZXRoaW5nIHdlbnQgd3JvbmcuPC9oMT47XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMucHJvcHMuY2hpbGRyZW47XG4gIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./components/atoms/ErrorBoundary/index.tsx\n");

/***/ }),

/***/ "./components/contexts/People.context.tsx":
/*!************************************************!*\
  !*** ./components/contexts/People.context.tsx ***!
  \************************************************/
/*! exports provided: PeopleContext, usePeopleContext, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PeopleContext\", function() { return PeopleContext; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"usePeopleContext\", function() { return usePeopleContext; });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n\nvar _jsxFileName = \"/Users/lukas.machado/Downloads/lukasferreira-react-coding-interview/components/contexts/People.context.tsx\";\n\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\nconst EDIT_ACTION = 'edit';\nconst APPEND_ACTION = 'append';\nconst INITIALIZE_ACTION = 'initialize';\n\nconst reducer = (state, action) => {\n  switch (action.type) {\n    case INITIALIZE_ACTION:\n      return _objectSpread(_objectSpread({}, state), {}, {\n        data: action.data,\n        initialized: true,\n        totalItems: action.totalItems\n      });\n\n    case EDIT_ACTION:\n      // this could have a better performance by storing a Map structure\n      // cotaining the person's email/id as key and value their position\n      //  on the array. That way the findIndex method is not necessary\n      const index = state.data.findIndex(person => person.email === action.data.email); // The state should never be modifiied directly\n      // Variant: Object.assign([], state.data, {[index]: newItem});\n\n      const copy = state.data.slice(0);\n      copy[index] = action.data;\n      return _objectSpread(_objectSpread({}, state), {}, {\n        data: copy\n      });\n    // items on the server (not available on the server)\n\n    case APPEND_ACTION:\n      return _objectSpread(_objectSpread({}, state), {}, {\n        data: [...state.data, ...action.data],\n        totalItems: action.totalItems\n      });\n\n    default:\n      throw new Error();\n  }\n};\n\nconst PeopleContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_1__[\"createContext\"])({\n  data: [],\n  initialized: false,\n  totalItems: 0,\n  hasMore: false,\n  append: () => {},\n  edit: () => {},\n  initialize: () => {}\n});\nconst usePeopleContext = () => Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useContext\"])(PeopleContext);\n\nconst PeopleProvider = ({\n  children\n}) => {\n  const {\n    0: state,\n    1: dispatch\n  } = Object(react__WEBPACK_IMPORTED_MODULE_1__[\"useReducer\"])(reducer, {\n    data: [],\n    initialized: false,\n    hasMore: false,\n    totalItems: 0\n  });\n  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(PeopleContext.Provider, {\n    value: _objectSpread(_objectSpread({}, state), {}, {\n      initialize: (data, totalItems) => dispatch({\n        data,\n        type: INITIALIZE_ACTION,\n        totalItems\n      }),\n      append: (data, totalItems) => dispatch({\n        data,\n        type: APPEND_ACTION,\n        totalItems\n      }),\n      edit: data => dispatch({\n        data,\n        type: EDIT_ACTION\n      })\n    }),\n    children: children\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 82,\n    columnNumber: 5\n  }, undefined);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (PeopleProvider);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL2NvbnRleHRzL1Blb3BsZS5jb250ZXh0LnRzeD85MmIzIl0sIm5hbWVzIjpbIkVESVRfQUNUSU9OIiwiQVBQRU5EX0FDVElPTiIsIklOSVRJQUxJWkVfQUNUSU9OIiwicmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsImRhdGEiLCJpbml0aWFsaXplZCIsInRvdGFsSXRlbXMiLCJpbmRleCIsImZpbmRJbmRleCIsInBlcnNvbiIsImVtYWlsIiwiY29weSIsInNsaWNlIiwiRXJyb3IiLCJQZW9wbGVDb250ZXh0IiwiY3JlYXRlQ29udGV4dCIsImhhc01vcmUiLCJhcHBlbmQiLCJlZGl0IiwiaW5pdGlhbGl6ZSIsInVzZVBlb3BsZUNvbnRleHQiLCJ1c2VDb250ZXh0IiwiUGVvcGxlUHJvdmlkZXIiLCJjaGlsZHJlbiIsImRpc3BhdGNoIiwidXNlUmVkdWNlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBSUEsTUFBTUEsV0FBVyxHQUFHLE1BQXBCO0FBQ0EsTUFBTUMsYUFBYSxHQUFHLFFBQXRCO0FBQ0EsTUFBTUMsaUJBQWlCLEdBQUcsWUFBMUI7O0FBcUJBLE1BQU1DLE9BQWdCLEdBQUcsQ0FBQ0MsS0FBRCxFQUFRQyxNQUFSLEtBQW1CO0FBQzFDLFVBQVFBLE1BQU0sQ0FBQ0MsSUFBZjtBQUNFLFNBQUtKLGlCQUFMO0FBQ0UsNkNBQ0tFLEtBREw7QUFFRUcsWUFBSSxFQUFFRixNQUFNLENBQUNFLElBRmY7QUFHRUMsbUJBQVcsRUFBRSxJQUhmO0FBSUVDLGtCQUFVLEVBQUVKLE1BQU0sQ0FBQ0k7QUFKckI7O0FBTUYsU0FBS1QsV0FBTDtBQUNFO0FBQ0E7QUFDQTtBQUNBLFlBQU1VLEtBQUssR0FBR04sS0FBSyxDQUFDRyxJQUFOLENBQVdJLFNBQVgsQ0FDWEMsTUFBRCxJQUFZQSxNQUFNLENBQUNDLEtBQVAsS0FBaUJSLE1BQU0sQ0FBQ0UsSUFBUCxDQUFZTSxLQUQ3QixDQUFkLENBSkYsQ0FPRTtBQUNBOztBQUNBLFlBQU1DLElBQUksR0FBR1YsS0FBSyxDQUFDRyxJQUFOLENBQVdRLEtBQVgsQ0FBaUIsQ0FBakIsQ0FBYjtBQUNBRCxVQUFJLENBQUNKLEtBQUQsQ0FBSixHQUFjTCxNQUFNLENBQUNFLElBQXJCO0FBQ0EsNkNBQVlILEtBQVo7QUFBbUJHLFlBQUksRUFBRU87QUFBekI7QUFDRjs7QUFDQSxTQUFLYixhQUFMO0FBQ0UsNkNBQ0tHLEtBREw7QUFFRUcsWUFBSSxFQUFFLENBQUMsR0FBR0gsS0FBSyxDQUFDRyxJQUFWLEVBQWdCLEdBQUdGLE1BQU0sQ0FBQ0UsSUFBMUIsQ0FGUjtBQUdFRSxrQkFBVSxFQUFFSixNQUFNLENBQUNJO0FBSHJCOztBQUtGO0FBQ0UsWUFBTSxJQUFJTyxLQUFKLEVBQU47QUE1Qko7QUE4QkQsQ0EvQkQ7O0FBaUNPLE1BQU1DLGFBQWEsZ0JBQUdDLDJEQUFhLENBQWlCO0FBQ3pEWCxNQUFJLEVBQUUsRUFEbUQ7QUFFekRDLGFBQVcsRUFBRSxLQUY0QztBQUd6REMsWUFBVSxFQUFFLENBSDZDO0FBSXpEVSxTQUFPLEVBQUUsS0FKZ0Q7QUFLekRDLFFBQU0sRUFBRSxNQUFNLENBQUUsQ0FMeUM7QUFNekRDLE1BQUksRUFBRSxNQUFNLENBQUUsQ0FOMkM7QUFPekRDLFlBQVUsRUFBRSxNQUFNLENBQUU7QUFQcUMsQ0FBakIsQ0FBbkM7QUFVQSxNQUFNQyxnQkFBZ0IsR0FBRyxNQUFNQyx3REFBVSxDQUFDUCxhQUFELENBQXpDOztBQUVQLE1BQU1RLGNBQWMsR0FBRyxDQUFDO0FBQUVDO0FBQUYsQ0FBRCxLQUFzRDtBQUMzRSxRQUFNO0FBQUEsT0FBQ3RCLEtBQUQ7QUFBQSxPQUFRdUI7QUFBUixNQUFvQkMsd0RBQVUsQ0FBQ3pCLE9BQUQsRUFBVTtBQUM1Q0ksUUFBSSxFQUFFLEVBRHNDO0FBRTVDQyxlQUFXLEVBQUUsS0FGK0I7QUFHNUNXLFdBQU8sRUFBRSxLQUhtQztBQUk1Q1YsY0FBVSxFQUFFO0FBSmdDLEdBQVYsQ0FBcEM7QUFPQSxzQkFDRSxxRUFBQyxhQUFELENBQWUsUUFBZjtBQUNFLFNBQUssa0NBQ0FMLEtBREE7QUFFSGtCLGdCQUFVLEVBQUUsQ0FBQ2YsSUFBRCxFQUFpQkUsVUFBakIsS0FDVmtCLFFBQVEsQ0FBQztBQUFFcEIsWUFBRjtBQUFRRCxZQUFJLEVBQUVKLGlCQUFkO0FBQWlDTztBQUFqQyxPQUFELENBSFA7QUFJSFcsWUFBTSxFQUFFLENBQUNiLElBQUQsRUFBaUJFLFVBQWpCLEtBQ05rQixRQUFRLENBQUM7QUFBRXBCLFlBQUY7QUFBUUQsWUFBSSxFQUFFTCxhQUFkO0FBQTZCUTtBQUE3QixPQUFELENBTFA7QUFNSFksVUFBSSxFQUFHZCxJQUFELElBQWtCb0IsUUFBUSxDQUFDO0FBQUVwQixZQUFGO0FBQVFELFlBQUksRUFBRU47QUFBZCxPQUFEO0FBTjdCLE1BRFA7QUFBQSxjQVVHMEI7QUFWSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFjRCxDQXRCRDs7QUF3QmVELDZFQUFmIiwiZmlsZSI6Ii4vY29tcG9uZW50cy9jb250ZXh0cy9QZW9wbGUuY29udGV4dC50c3guanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgY3JlYXRlQ29udGV4dCwgdXNlQ29udGV4dCwgdXNlUmVkdWNlciB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IElDb250ZXh0RGF0YSwgSUNvbnRleHRQcm92aWRlclByb3BzIH0gZnJvbSAnLi90eXBlcy5jb250ZXh0JztcbmltcG9ydCB7IFBlcnNvbiB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy90eXBlcyc7XG5cbmNvbnN0IEVESVRfQUNUSU9OID0gJ2VkaXQnO1xuY29uc3QgQVBQRU5EX0FDVElPTiA9ICdhcHBlbmQnO1xuY29uc3QgSU5JVElBTElaRV9BQ1RJT04gPSAnaW5pdGlhbGl6ZSc7XG5cbnR5cGUgQ29udGV4dERhdGEgPSBQZXJzb25bXTtcbmludGVyZmFjZSBQZW9wbGVTdGF0ZSBleHRlbmRzIElDb250ZXh0RGF0YTxDb250ZXh0RGF0YT4ge1xuICBoYXNNb3JlOiBib29sZWFuO1xuICB0b3RhbEl0ZW1zOiBudW1iZXI7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVBlb3BsZUNvbnRleHQgZXh0ZW5kcyBQZW9wbGVTdGF0ZSB7XG4gIGVkaXQ6IChkYXRhOiBQZXJzb24pID0+IHZvaWQ7XG4gIGFwcGVuZDogKGRhdGE6IFBlcnNvbltdLCB0b3RhbEl0ZW1zOiBudW1iZXIpID0+IHZvaWQ7XG4gIGluaXRpYWxpemU6IChkYXRhOiBQZXJzb25bXSwgdG90YWxJdGVtczogbnVtYmVyKSA9PiB2b2lkO1xufVxuXG50eXBlIEFjdGlvbiA9XG4gIHwgeyB0eXBlOiAnZWRpdCc7IGRhdGE6IFBlcnNvbiB9XG4gIHwgeyB0eXBlOiAnYXBwZW5kJzsgZGF0YTogUGVyc29uW107IHRvdGFsSXRlbXM6IG51bWJlciB9XG4gIHwgeyB0eXBlOiAnaW5pdGlhbGl6ZSc7IGRhdGE6IFBlcnNvbltdOyB0b3RhbEl0ZW1zOiBudW1iZXIgfTtcblxudHlwZSBSZWR1Y2VyID0gKHByZXZTdGF0ZTogUGVvcGxlU3RhdGUsIGFjdGlvbjogQWN0aW9uKSA9PiBQZW9wbGVTdGF0ZTtcblxuY29uc3QgcmVkdWNlcjogUmVkdWNlciA9IChzdGF0ZSwgYWN0aW9uKSA9PiB7XG4gIHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcbiAgICBjYXNlIElOSVRJQUxJWkVfQUNUSU9OOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIGRhdGE6IGFjdGlvbi5kYXRhLFxuICAgICAgICBpbml0aWFsaXplZDogdHJ1ZSxcbiAgICAgICAgdG90YWxJdGVtczogYWN0aW9uLnRvdGFsSXRlbXMsXG4gICAgICB9O1xuICAgIGNhc2UgRURJVF9BQ1RJT046XG4gICAgICAvLyB0aGlzIGNvdWxkIGhhdmUgYSBiZXR0ZXIgcGVyZm9ybWFuY2UgYnkgc3RvcmluZyBhIE1hcCBzdHJ1Y3R1cmVcbiAgICAgIC8vIGNvdGFpbmluZyB0aGUgcGVyc29uJ3MgZW1haWwvaWQgYXMga2V5IGFuZCB2YWx1ZSB0aGVpciBwb3NpdGlvblxuICAgICAgLy8gIG9uIHRoZSBhcnJheS4gVGhhdCB3YXkgdGhlIGZpbmRJbmRleCBtZXRob2QgaXMgbm90IG5lY2Vzc2FyeVxuICAgICAgY29uc3QgaW5kZXggPSBzdGF0ZS5kYXRhLmZpbmRJbmRleChcbiAgICAgICAgKHBlcnNvbikgPT4gcGVyc29uLmVtYWlsID09PSBhY3Rpb24uZGF0YS5lbWFpbFxuICAgICAgKTtcbiAgICAgIC8vIFRoZSBzdGF0ZSBzaG91bGQgbmV2ZXIgYmUgbW9kaWZpaWVkIGRpcmVjdGx5XG4gICAgICAvLyBWYXJpYW50OiBPYmplY3QuYXNzaWduKFtdLCBzdGF0ZS5kYXRhLCB7W2luZGV4XTogbmV3SXRlbX0pO1xuICAgICAgY29uc3QgY29weSA9IHN0YXRlLmRhdGEuc2xpY2UoMCk7XG4gICAgICBjb3B5W2luZGV4XSA9IGFjdGlvbi5kYXRhO1xuICAgICAgcmV0dXJuIHsgLi4uc3RhdGUsIGRhdGE6IGNvcHkgfTtcbiAgICAvLyBpdGVtcyBvbiB0aGUgc2VydmVyIChub3QgYXZhaWxhYmxlIG9uIHRoZSBzZXJ2ZXIpXG4gICAgY2FzZSBBUFBFTkRfQUNUSU9OOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIGRhdGE6IFsuLi5zdGF0ZS5kYXRhLCAuLi5hY3Rpb24uZGF0YV0sXG4gICAgICAgIHRvdGFsSXRlbXM6IGFjdGlvbi50b3RhbEl0ZW1zLFxuICAgICAgfTtcbiAgICBkZWZhdWx0OlxuICAgICAgdGhyb3cgbmV3IEVycm9yKCk7XG4gIH1cbn07XG5cbmV4cG9ydCBjb25zdCBQZW9wbGVDb250ZXh0ID0gY3JlYXRlQ29udGV4dDxJUGVvcGxlQ29udGV4dD4oe1xuICBkYXRhOiBbXSxcbiAgaW5pdGlhbGl6ZWQ6IGZhbHNlLFxuICB0b3RhbEl0ZW1zOiAwLFxuICBoYXNNb3JlOiBmYWxzZSxcbiAgYXBwZW5kOiAoKSA9PiB7fSxcbiAgZWRpdDogKCkgPT4ge30sXG4gIGluaXRpYWxpemU6ICgpID0+IHt9LFxufSk7XG5cbmV4cG9ydCBjb25zdCB1c2VQZW9wbGVDb250ZXh0ID0gKCkgPT4gdXNlQ29udGV4dChQZW9wbGVDb250ZXh0KTtcblxuY29uc3QgUGVvcGxlUHJvdmlkZXIgPSAoeyBjaGlsZHJlbiB9OiBJQ29udGV4dFByb3ZpZGVyUHJvcHM8Q29udGV4dERhdGE+KSA9PiB7XG4gIGNvbnN0IFtzdGF0ZSwgZGlzcGF0Y2hdID0gdXNlUmVkdWNlcihyZWR1Y2VyLCB7XG4gICAgZGF0YTogW10sXG4gICAgaW5pdGlhbGl6ZWQ6IGZhbHNlLFxuICAgIGhhc01vcmU6IGZhbHNlLFxuICAgIHRvdGFsSXRlbXM6IDAsXG4gIH0pO1xuXG4gIHJldHVybiAoXG4gICAgPFBlb3BsZUNvbnRleHQuUHJvdmlkZXJcbiAgICAgIHZhbHVlPXt7XG4gICAgICAgIC4uLnN0YXRlLFxuICAgICAgICBpbml0aWFsaXplOiAoZGF0YTogUGVyc29uW10sIHRvdGFsSXRlbXM6IG51bWJlcikgPT5cbiAgICAgICAgICBkaXNwYXRjaCh7IGRhdGEsIHR5cGU6IElOSVRJQUxJWkVfQUNUSU9OLCB0b3RhbEl0ZW1zIH0pLFxuICAgICAgICBhcHBlbmQ6IChkYXRhOiBQZXJzb25bXSwgdG90YWxJdGVtczogbnVtYmVyKSA9PlxuICAgICAgICAgIGRpc3BhdGNoKHsgZGF0YSwgdHlwZTogQVBQRU5EX0FDVElPTiwgdG90YWxJdGVtcyB9KSxcbiAgICAgICAgZWRpdDogKGRhdGE6IFBlcnNvbikgPT4gZGlzcGF0Y2goeyBkYXRhLCB0eXBlOiBFRElUX0FDVElPTiB9KSxcbiAgICAgIH19XG4gICAgPlxuICAgICAge2NoaWxkcmVufVxuICAgIDwvUGVvcGxlQ29udGV4dC5Qcm92aWRlcj5cbiAgKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFBlb3BsZVByb3ZpZGVyO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./components/contexts/People.context.tsx\n");

/***/ }),

/***/ "./node_modules/antd/dist/antd.css":
/*!*****************************************!*\
  !*** ./node_modules/antd/dist/antd.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIuL25vZGVfbW9kdWxlcy9hbnRkL2Rpc3QvYW50ZC5jc3MuanMiLCJzb3VyY2VzQ29udGVudCI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./node_modules/antd/dist/antd.css\n");

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/dist/antd.css */ \"./node_modules/antd/dist/antd.css\");\n/* harmony import */ var antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/globals.css */ \"./styles/globals.css\");\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _components_contexts_People_context__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/contexts/People.context */ \"./components/contexts/People.context.tsx\");\n/* harmony import */ var _components_atoms_ErrorBoundary__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/atoms/ErrorBoundary */ \"./components/atoms/ErrorBoundary/index.tsx\");\n\nvar _jsxFileName = \"/Users/lukas.machado/Downloads/lukasferreira-react-coding-interview/pages/_app.js\";\n\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\n\n\n\n\nconst MyApp = ({\n  Component,\n  pageProps\n}) => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(_components_atoms_ErrorBoundary__WEBPACK_IMPORTED_MODULE_5__[\"default\"], {\n  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(_components_contexts_People_context__WEBPACK_IMPORTED_MODULE_4__[\"default\"], {\n    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__[\"jsxDEV\"])(Component, _objectSpread({}, pageProps), void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 11,\n      columnNumber: 7\n    }, undefined)\n  }, void 0, false, {\n    fileName: _jsxFileName,\n    lineNumber: 10,\n    columnNumber: 5\n  }, undefined)\n}, void 0, false, {\n  fileName: _jsxFileName,\n  lineNumber: 9,\n  columnNumber: 3\n}, undefined);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (MyApp);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9fYXBwLmpzP2Q1MzAiXSwibmFtZXMiOlsiTXlBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFFQSxNQUFNQSxLQUFLLEdBQUcsQ0FBQztBQUFFQyxXQUFGO0FBQWFDO0FBQWIsQ0FBRCxrQkFDWixxRUFBQyx1RUFBRDtBQUFBLHlCQUNFLHFFQUFDLDJFQUFEO0FBQUEsMkJBQ0UscUVBQUMsU0FBRCxvQkFBZUEsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFERjs7QUFRZUYsb0VBQWYiLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICdhbnRkL2Rpc3QvYW50ZC5jc3MnO1xuaW1wb3J0ICcuLi9zdHlsZXMvZ2xvYmFscy5jc3MnO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcblxuaW1wb3J0IFBlb3BsZVByb3ZpZGVyIGZyb20gJy4uL2NvbXBvbmVudHMvY29udGV4dHMvUGVvcGxlLmNvbnRleHQnO1xuaW1wb3J0IEVycm9yQm91bmRhcnkgZnJvbSAnLi4vY29tcG9uZW50cy9hdG9tcy9FcnJvckJvdW5kYXJ5JztcblxuY29uc3QgTXlBcHAgPSAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9KSA9PiAoXG4gIDxFcnJvckJvdW5kYXJ5PlxuICAgIDxQZW9wbGVQcm92aWRlcj5cbiAgICAgIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz5cbiAgICA8L1Blb3BsZVByb3ZpZGVyPlxuICA8L0Vycm9yQm91bmRhcnk+XG4pO1xuXG5leHBvcnQgZGVmYXVsdCBNeUFwcDtcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiIuL3N0eWxlcy9nbG9iYWxzLmNzcy5qcyIsInNvdXJjZXNDb250ZW50IjpbXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./styles/globals.css\n");

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "antd":
/*!***********************!*\
  !*** external "antd" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"antd\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJhbnRkXCI/MDhhYSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJhbnRkLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYW50ZFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///antd\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiPzU4OGUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoicmVhY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react/jsx-dev-runtime\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIj9jZDkwIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6InJlYWN0L2pzeC1kZXYtcnVudGltZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///react/jsx-dev-runtime\n");

/***/ })

/******/ });